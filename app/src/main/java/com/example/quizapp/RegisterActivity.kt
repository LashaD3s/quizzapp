package com.example.quizapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
        d("method", "onCreate")
    }
    override fun onStart() {
        super.onStart()
        d("method", "onStart")
    }
    override fun onResume() {
        super.onResume()
        d("method", "onResume")
    }
    override fun onPause() {
        super.onPause()
        d("method", "onPause")
    }
    override fun onStop() {
        super.onStop()
        d("method", "onStop")
    }
    override fun onRestart() {
        super.onRestart()
        d("method", "onRestart")
    }
    override fun onDestroy() {
        super.onDestroy()
        d("method", "onDestroy")
    }
    private fun init() {
        auth = Firebase.auth
        btn_register_button.setOnClickListener {
            signUp()
        }
    }
    private fun signUp() {
        val mail = et_register_email.text.toString()
        val password = et_register_password.text.toString()
        val repeatpassword = et_register_password_repeat.text.toString()

        if (mail.isNotEmpty() &&
            password.isNotEmpty() && repeatpassword.isNotEmpty()
        ) {
            if (password == repeatpassword) {
                deleteClick(true)
                auth.createUserWithEmailAndPassword(mail, password)
                    .addOnCompleteListener(this) { task ->
                        deleteClick(false)
                        if (task.isSuccessful) {
                            d("Sign Up Success", "createUserWithEmail:success")
                            val user = auth.currentUser
                        } else {
                            Log.w("Sign Up Failed", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(
                                baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }
        }
        openMainActivity()
    }
    private fun deleteClick(isStarted:Boolean){
        btn_register_button.isClickable = !isStarted
        if(isStarted)
            register_progress_bar.visibility = View.VISIBLE
        else
            register_progress_bar.visibility = View.GONE
    }

    private fun openMainActivity(){
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}