package com.example.quizapp
object Constants{
    const val USER_NAME: String = "user_name"
    const val TOTAL_QUESTIONS: String = "total_question"
    const val CORRECT_ANWERS: String = "correct_answers"
    fun getQuestions(): ArrayList<Question>{
        val questionsList = ArrayList<Question>()
        val question1 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.australia,
            "Argentina",
            "Australia",
            "Armenia",
            "Austria",
            2)
        questionsList.add(question1)
        val question2 = Question(
            2,
            "What country does this flag belong to?",
            R.drawable.canada,
            "Canada",
            "China",
            "USA",
            "Spain",
            1)
        questionsList.add(question2)
        val question3 = Question(
            3,
            "What country does this flag belong to?",
            R.drawable.china,
            "Canada",
            "Austria",
            "France",
            "China",
            4)
        questionsList.add(question3)
        val question4 = Question(
            4,
            "What country does this flag belong to?",
            R.drawable.france,
            "Russia",
            "Germany",
            "France",
            "Belgium",
            3)
        questionsList.add(question4)
        val question5 = Question(
            5,
            "What country does this flag belong to?",
            R.drawable.georgia,
            "Georgia",
            "Denmark",
            "Iceland",
            "Norway",
            1)
        questionsList.add(question5)
        val question6 = Question(
            6,
            "What country does this flag belong to?",
            R.drawable.germany,
            "Russia",
            "Germany",
            "France",
            "Belgium",
            2)
        questionsList.add(question6)
        val question7 = Question(
            7,
            "What country does this flag belong to?",
            R.drawable.japan,
            "Argentina",
            "Japan",
            "Bangladesh",
            "India",
            2)
        questionsList.add(question7)
        val question8 = Question(
            8,
            "What country does this flag belong to?",
            R.drawable.spain,
            "Spain",
            "Portugal",
            "Peru",
            "Mexico",
            1)
        questionsList.add(question8)
        val question9 = Question(
            9,
            "What country does this flag belong to?",
            R.drawable.uk,
            "United Kingdom",
            "United States",
            "Italy",
            "Australia",
            1)
        questionsList.add(question9)
        val question10 = Question(
            10,
            "What country does this flag belong to?",
            R.drawable.us,
            "Liberia",
            "United Kingdom",
            "United States",
            "New Zealand",
            3)
        questionsList.add(question10)
        return questionsList
    }
}