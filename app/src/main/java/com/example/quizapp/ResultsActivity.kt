package com.example.quizapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_reults.*

class ResultsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reults)
        val userName = intent.getStringArrayExtra(Constants.USER_NAME)
        val totalQuestions = intent.getIntExtra(Constants.TOTAL_QUESTIONS, 0)
        val correctAnswer = intent.getIntExtra(Constants.CORRECT_ANWERS, 0)
        tv_score.text = "Your Score is $correctAnswer out of $totalQuestions"
        btn_finish.setOnClickListener{
            startActivity(Intent(this, LogInActivity::class.java))
        }
        btn_try_again.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}